package zw.co.econet.employeesystem.Repository;

import org.hibernate.mapping.List;
import org.springframework.data.jpa.repository.JpaRepository;
import zw.co.econet.employeesystem.Model.User;


import java.util.Optional;

public interface UserRepository extends  JpaRepository<User, Long>{

}
