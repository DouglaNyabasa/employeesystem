package zw.co.econet.employeesystem.Controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import zw.co.econet.employeesystem.Model.User;
import zw.co.econet.employeesystem.Service.UserService;

import java.util.List;

@RestController
@RequestMapping("/api/users")
public class UserController {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }



    @PostMapping("/save")
        public ResponseEntity<User> saveUser(@RequestBody User user){
        return new ResponseEntity<User>(userService.saveUser(user), HttpStatus.CREATED);

    }
    @GetMapping("/all")
    public List<User> getAllUsers(){
        return userService.getAllUser();

    }
    @GetMapping("{id}")
    public ResponseEntity<User>findById(@PathVariable("id") long usersId){
        return new ResponseEntity<User>(userService.getUserbyId(usersId),HttpStatus.OK);
    }

}
