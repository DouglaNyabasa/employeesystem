package zw.co.econet.employeesystem.Service;

import org.springframework.stereotype.Service;
import zw.co.econet.employeesystem.Model.User;
import zw.co.econet.employeesystem.Repository.UserRepository;

import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService{

    private UserRepository userRepository;

    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public User saveUser(User user) {
        return userRepository.save(user);
    }

    @Override
    public List<User> getAllUser() {
        return userRepository.findAll();
    }

    @Override
    public User getUserbyId(long Id) {
       Optional <User> user = userRepository.findById(Id);
           return user.get();

    }

    }


