package zw.co.econet.employeesystem.Service;

import org.springframework.stereotype.Service;
import zw.co.econet.employeesystem.Model.User;
import zw.co.econet.employeesystem.Model.User;
import zw.co.econet.employeesystem.Repository.UserRepository;

import javax.persistence.Id;
import java.util.List;
import java.util.Optional;


public interface UserService {
    User saveUser(User users);
    List<User>getAllUser();
    User getUserbyId(long Id);

}
