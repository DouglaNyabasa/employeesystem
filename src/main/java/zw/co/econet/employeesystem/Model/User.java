package zw.co.econet.employeesystem.Model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "Users")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "First_Name")
    private  String FirstName;

    @Column(name = "Second_Name")
    private  String SecondName;

    @Column(name = "email")
    private  String email;

    @Column(name = "PhoneNumber")
    private  String MobileNumber;

    //public users() {}
}
